import React, { useState, ChangeEvent } from 'react';
import { Typography, Grid } from '@mui/material';
import BoxList from './BoxList'
import { ITask } from '../Interfaces'
import AddIcon from '@mui/icons-material/Add';
import ChangeData from './ChangeData'
import { Co2Sharp } from '@mui/icons-material';


function Home() {
    const [task, setTask] = useState<string>("")
    const [todoList, setTodoList] = useState<ITask[]>([])
    const [addMainBox, setAddMainBox] = useState<boolean>(false)
    const handleChange = (event: ChangeEvent<HTMLInputElement>): void => {
        setTask(event.target.value)
    }
    const clickCloseAccordion = ():void=>{
        setAddMainBox(false)
    }
    const addTask = (): void => {
        const newTask = { taskName: task }
        setTodoList([...todoList, newTask])
        setTask("")
        setAddMainBox(false)
    }
    const compeleteTask = (taskListDelete: string): void => {
        setTodoList(todoList.filter(task => {
            return task.taskName != taskListDelete
        }))
    }
    const clickAccordion = (): void => {
        setAddMainBox(true)
    }
console.log(addMainBox)
    return (
        <div className="container-list">
            <div><h3 >مسیرهای ارتباطی</h3></div>
            <div className='btn-add-list'>
                <button type='button' onClick={clickAccordion} ><span><AddIcon /></span>اضافه کردن لیست</button>
                {addMainBox ?
                    <BoxList addTask={addTask} clickCloseAccordion={clickCloseAccordion} handleChange={handleChange} setTask={setTask} />
                    : null}

            </div>
            <div>
                {todoList.map((task: ITask, key: number) => {
                    return (
                        <>
                            <ChangeData key={key} task={task} compeleteTask={compeleteTask} />
                  </>)
                })}
            </div>
        </div>
    );
}

export default Home;
