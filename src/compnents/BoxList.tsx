import React, { FC, ChangeEvent, useState, useEffect } from 'react'
import { Typography, Grid } from '@mui/material';
import { Button } from 'react-bootstrap'
import TextField, { TextFieldProps } from '@mui/material/TextField';
import { styled, makeStyles } from '@mui/material/styles';
import Autocomplete from '@mui/material/Autocomplete';
import TwitterIcon from '@mui/icons-material/Twitter';
import FacebookIcon from '@mui/icons-material/Facebook';
import InstagramIcon from '@mui/icons-material/Instagram';
import TelegramIcon from '@mui/icons-material/Telegram';
import Paper from "@mui/material/Paper";

import classnames from 'classnames'
const CssTextField = styled(TextField)({
  '& label.Mui-focused': {
    color: '#C69C2E',
  },
  '& .MuiInput-underline:after': {
    borderBottomColor: '#C69C2E',
  },
  '& .MuiOutlinedInput-root': {
    '& fieldset': {
      borderColor: '#6B7279',
    },
    '&:hover fieldset': {
      borderColor: '#fff',
    },
    '&.Mui-focused fieldset': {
      borderColor: '#C69C2E',
    },

  },
});

 const CssAtocom =styled(Autocomplete)({
  '& label.Mui-focused': {
    color: '#C69C2E',
  },
  '& .MuiInput-underline:after': {
    borderBottomColor: '#C69C2E',
  },
  '& .MuiOutlinedInput-root': {
    '& fieldset': {
      borderColor: '#6B7279',
    },
    '&:hover fieldset': {
      borderColor: '#fff',
    },
    '&.Mui-focused fieldset': {
      borderColor: '#C69C2E',
    },

  },
 })
interface BioProps {
  addTask: () => void,
  handleChange: (event: ChangeEvent<HTMLInputElement>) => void,
  setTask: React.Dispatch<React.SetStateAction<any>>,
  clickCloseAccordion:()=> void

}
const BoxList = ({ addTask, handleChange, setTask,clickCloseAccordion }: BioProps) => {
  const [value, setValue] = useState<any | null>(null)
  const [changeInputConditional, setChangeInputConditional] = useState<boolean>(false) // this state for conditional disabled or not for button
  const [valueConditional, setValueConditional] = useState<any>("")
  const dataSocial = [
    { label: 'تویتر', logo: <TwitterIcon /> },
    { label: 'فیسبوک', logo: <FacebookIcon /> },
    { label: 'اینستاگرام', logo: <InstagramIcon /> },
    { label: 'تلگرام', logo: <TelegramIcon /> },
  ]

  const handleChangeInput = (event: ChangeEvent<HTMLInputElement>): void => {
    setTask(event.target.value)
    setChangeInputConditional(true)
    setValueConditional(event.target.value)
  }
  console.log(value)
  return (
    <div className="box-list-container">
      <Typography >مسیرهای ارتباطی</Typography>
      <div className='atucomplete-main'>
        <Autocomplete
          disablePortal
          id="combo-box-demo"
          options={dataSocial}
          getOptionLabel={(option) => option.label}
          sx={{ width: 300 }}
          PaperComponent={({ children }) => (
            <Paper style={{ background: "#161B25", color: '#fff' }}>{children}</Paper>
          )}
          renderOption={(props, option) => {
            const { logo, label } = option;
            return (
              <div className='logo-input'>
                <span {...props} >
                  {logo}
                </span>
                <span {...props} >
                  {label}
                </span>
              </div>
            );
          }}
          renderInput={(params) => <TextField {...params} label="Movie" />}
        />
        <CssTextField label="addListAddres" id="custom-css-outlined-input" onChange={handleChangeInput} />
      </div>
      <div className='btn-group-add'>
        <button className="optout-btn" type='button' onClick={clickCloseAccordion}>انصراف</button>
        <button onClick={addTask} disabled={valueConditional.length > 0 ? false : true} className={classnames(valueConditional.length >0 ? " optout-btn" : ' disabled-icon disabled-icon-submit')} type='button' ><span>{ }</span> ثبت مسیر ارتباطی </button>
      </div>
    </div>
  )
}

export default BoxList;