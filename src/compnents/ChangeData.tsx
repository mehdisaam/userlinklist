import React, { useState, ChangeEvent } from 'react'
import TextField, { TextFieldProps } from '@mui/material/TextField';
import { styled, makeStyles } from '@mui/material/styles';
import Autocomplete from '@mui/material/Autocomplete';
import TwitterIcon from '@mui/icons-material/Twitter';
import FacebookIcon from '@mui/icons-material/Facebook';
import InstagramIcon from '@mui/icons-material/Instagram';
import TelegramIcon from '@mui/icons-material/Telegram';
import Paper from "@mui/material/Paper";

import { Typography, Grid } from '@mui/material';
import { ITask } from '../Interfaces'
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import classnames from 'classnames'
interface Props {
  task: ITask,
  compeleteTask(taskListDelete: string): void
}
const CssTextField = styled(TextField)({
  '& label.Mui-focused': {
    color: '#C69C2E',
  },
  '& .MuiInput-underline:after': {
    borderBottomColor: '#C69C2E',
  },
  '& .MuiOutlinedInput-root': {
    '& fieldset': {
      borderColor: '#6B7279',
    },
    '&:hover fieldset': {
      borderColor: '#fff',
    },
    '&.Mui-focused fieldset': {
      borderColor: '#C69C2E',
    },

  },

});

const ChangeData = ({ task, compeleteTask }: Props) => {
  const [accordion, setAcourdion] = useState<boolean>(false)
  const [submitForm, setSubmitForm] = useState<boolean>(false)
  const [changeInputConditional, setChangeInputConditional] = useState<boolean>(false) // this state for conditional disabled or not for button
  const [valueConditional, setValueConditional] = useState<any>(task?.taskName)
  const dataSocial = [
    { label: 'تویتر', logo: <TwitterIcon /> },
    { label: 'فیسبوک', logo: <FacebookIcon /> },
    { label: 'اینستاگرام', logo: <InstagramIcon /> },
    { label: 'تلگرام', logo: <TelegramIcon /> },
  ]
  const handleChange = (event: ChangeEvent<HTMLInputElement>): void => {
    setChangeInputConditional(true)
    setValueConditional(event.target.value)
  }

  const openAccordion = (): void => {
    setAcourdion(true)

  }
  const closeAccordion = (): void =>{
    setAcourdion(false)
  }
  const submitFormInput = (): void => {
    setSubmitForm(true)
    setAcourdion(false)
  }

  console.log(valueConditional.length)
  return (
    <div className="box-list-container">
      <div className="header-box">
        <div className='right-header'>
          <p>لینک</p>
          <div className='header-text'>{valueConditional}</div>

        </div>
        <div className="btn-group-data">
          <button className='btn-edite-box' onClick={() => compeleteTask(task.taskName)}><span><DeleteIcon /></span><p>حذف</p></button>
          <button className={classnames(accordion ? 'disabled-icon' : "btn-edite-box")} onClick={openAccordion} disabled={accordion ? true : false}> <span><EditIcon /></span><p>ویرایش</p></button>
        </div>
      </div>
      {accordion ?
        <div className='content-main-box'>
          <Typography > ویرایش مسیرهای ارتباطی</Typography>
          <div className='atucomplete-main'>
            <Autocomplete
              disablePortal
              id="combo-box-demo"
              options={dataSocial}
              getOptionLabel={(option) => option.label}
              sx={{ width: 300, hover: 'red' }}
              PaperComponent={({ children }) => (
                <Paper style={{ background: "#161B25", color: '#fff' }}>{children}</Paper>
              )}
              renderOption={(props, option) => {
                const { logo, label } = option;
                return (
                  <div className='logo-input'>
                    <span {...props} >
                      {logo}
                    </span>
                    <span {...props} >
                      {label}
                    </span>
                  </div>
                );
              }}
              renderInput={(params) => <TextField {...params} label="Movie" />}
            />
            <CssTextField label="addListAddres" id="custom-css-outlined-input" onChange={handleChange} value={valueConditional} />
          </div>
          <div className='btn-group-add'>
            <button className="optout-btn" type='button' onClick={closeAccordion}>انصراف</button>
            <button className={classnames(changeInputConditional ? " optout-btn" : ' disabled-icon disabled-icon-submit')} type='button' onClick={submitFormInput}><span>{ }</span> ثبت مسیر ارتباطی </button>
          </div>
        </div> : null

      }
    </div>
  )
}

export default ChangeData;